package com.vanessayau.statistics.models;

import com.google.gson.annotations.Expose;

/**
 * Created by Vanessa_CU on 12/05/2015.
 */
public class StreetRecord {
    @Expose
    private String id,
            name;

    public StreetRecord(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.vanessayau.statistics.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

/**
 * Created by Vanessa_CU on 12/05/2015.
 */
public class CrimeRecord {
    @Expose
    private String category,
            location_type,
            context,
            persistent_id,
            id,
            location_subtype,
            month;

    @Expose
    private OutcomeStatusRecord outcome_status;

    @Expose
    private LocationRecord location;

    public String getCategory() {
        String temp_category = category.replace('-', ' ');
        return Character.toUpperCase(temp_category.charAt(0)) + temp_category.substring(1);
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation_type() {
        return location_type;
    }

    public void setLocation_type(String location_type) {
        this.location_type = location_type;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getPersistent_id() {
        return persistent_id;
    }

    public void setPersistent_id(String persistent_id) {
        this.persistent_id = persistent_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation_subtype() {
        return location_subtype;
    }

    public void setLocation_subtype(String location_subtype) {
        this.location_subtype = location_subtype;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public OutcomeStatusRecord getOutcome_status() {
        return outcome_status;
    }

    public void setOutcome_status(OutcomeStatusRecord outcome_status) {
        this.outcome_status = outcome_status;
    }

    public LocationRecord getLocation() {
        return location;
    }

    public void setLocation(LocationRecord location) {
        this.location = location;
    }
}

package com.vanessayau.statistics.models;

import com.google.gson.annotations.Expose;

/**
 * Created by Vanessa_CU on 12/05/2015.
 */
public class LocationRecord {
    @Expose
    private String latitude,
            longitude;

    @Expose
    private StreetRecord street;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public StreetRecord getStreet() {
        return street;
    }

    public void setStreet(StreetRecord street) {
        this.street = street;
    }
}

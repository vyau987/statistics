package com.vanessayau.statistics.models;

import com.google.gson.annotations.Expose;

/**
 * Created by Vanessa_CU on 12/05/2015.
 */
public class OutcomeStatusRecord {
    @Expose
    private String category,
            date;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

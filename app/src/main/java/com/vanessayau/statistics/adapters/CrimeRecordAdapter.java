package com.vanessayau.statistics.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.vanessayau.statistics.R;
import com.vanessayau.statistics.models.CrimeRecord;
import com.vanessayau.statistics.models.OutcomeStatusRecord;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vanessa_CU on 12/05/2015.
 */
public class CrimeRecordAdapter extends ArrayAdapter<CrimeRecord> {
    private Activity context;
    private CrimeRecord[] crimeRecords;

    public CrimeRecordAdapter(Activity context, CrimeRecord[] crimeRecords) {
        super(context, R.layout.row_crime_record, crimeRecords);

        this.context = context;
        this.crimeRecords = crimeRecords;
    }

    @Override
    public View getView(int position, View crimeRecordRow, ViewGroup parent) {
        // reuse the existing view if it already exists
        if( null == crimeRecordRow ) {
            LayoutInflater layoutInflater = this.context.getLayoutInflater();
            crimeRecordRow = layoutInflater.inflate(R.layout.row_crime_record, parent, false);
        }

        // grab elements in the row and add the text
        crimeRecordRow.findViewById(R.id.row_crime_record_location);

        // get the strings and textviews to be manipulated
        final TextView crimeCategoryView = (TextView) crimeRecordRow.findViewById(R.id.row_crime_record_category);
        final TextView crimeLocationView = (TextView) crimeRecordRow.findViewById(R.id.row_crime_record_location);
        final TextView crimeDateOutcomeView = (TextView) crimeRecordRow.findViewById(R.id.row_crime_record_date_outcome);

        CrimeRecord crimeRecord = this.crimeRecords[position];
        String category     = crimeRecord.getCategory();
        String street       = crimeRecord.getLocation().getStreet().getName();

        // check strings are valid
        if( null == category || "".equals(category) ) {
            category = "category";
        }

        if( null == street || "".equals(street) ) {
            street = "street";
        }

        // concatenate the date-status for a crime record
        OutcomeStatusRecord outcomeStatus = crimeRecord.getOutcome_status();
        String dateOutcome;
        if( null == outcomeStatus || "".equals(outcomeStatus) ) {
            dateOutcome = "Outcome unknown";
        } else {
            StringBuilder stringBuilder = new StringBuilder(crimeRecord.getOutcome_status().getDate());
            stringBuilder.append(" - ").append(crimeRecord.getOutcome_status().getCategory());
            dateOutcome = stringBuilder.toString();
        }

        // for laziness use a map to populate the fields and iterate over it
        // populating crime record rows if information available
        HashMap<String, TextView> outputFields = new HashMap<String, TextView>();
        outputFields.put(dateOutcome, crimeDateOutcomeView);
        outputFields.put(category, crimeCategoryView);
        outputFields.put(street, crimeLocationView);

        for( Map.Entry<String, TextView> crimeInfo : outputFields.entrySet() ) {
            crimeInfo.getValue().setText(crimeInfo.getKey());
        }
        return crimeRecordRow;
    }

}

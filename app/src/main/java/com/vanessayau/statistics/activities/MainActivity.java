package com.vanessayau.statistics.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vanessayau.statistics.PolygonBuilder;
import com.vanessayau.statistics.R;
import com.vanessayau.statistics.adapters.CrimeRecordAdapter;
import com.vanessayau.statistics.models.CrimeRecord;
import com.vanessayau.statistics.request_helpers.UrlBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends ActionBarActivity {
    private final MainActivity thisActivity = this;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private String defaultDate = "2013-01";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set default search date
        final EditText dateText = (EditText) findViewById(R.id.crime_by_date_input);
        dateText.setText(defaultDate);

        dateFormatter = new SimpleDateFormat("yyyy-MM", Locale.ENGLISH);
        setupDatePicker();

        Button dateButton = (Button) findViewById(R.id.button_date_input);
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        // set button click listener to make request to data.police API for crime records
        Button makeRequestButton = (Button) findViewById(R.id.button_make_request);
        makeRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText dateInputField = (EditText) findViewById(R.id.crime_by_date_input);
                System.out.println("Date given: " + dateInputField.getText());
                makeRequest();
            }
        });
    }

    public void setupDatePicker() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar inputDate = Calendar.getInstance();
                inputDate.set(year, monthOfYear, dayOfMonth);

                // little hack to get months to display correctly (January displays as 0 instead of 01)
                String month = String.valueOf(inputDate.get(Calendar.MONTH) + 1);
                if( 1 == month.length() ) {
                    month = "0".concat(month);
                }
                Log.d("INPUT DATE AS STRING", String.valueOf(inputDate));

                // set the edit text field
                final EditText dateText = (EditText) findViewById(R.id.crime_by_date_input);
                dateText.setText(String.valueOf(inputDate.get(Calendar.YEAR)) + "-" + month);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    private void makeRequest() {
        if( connectionOk() ) {
            String url = "crimes-street/all-crime";
            // default entries
            //HashMap<String, String> params = getRequestParameters();

            // default custom url
//            ArrayList<String> latLngList = (new PolygonBuilder("0.543","52.794")).getLatLngList();
//            HashMap<String, String> params = getCustomRequestParameters(latLngList);

            HashMap<String,String> params = getCustomRequestParameters(null);
            if( null != params ) {
                String fullUrl = (new UrlBuilder(url, params).toString());
                Log.d("FULL REQUEST URL", fullUrl);
                new GetRequestTask().execute(fullUrl);
            } else {
                Toast.makeText(this, "Malformed request, this should not happen.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "No network connection available.", Toast.LENGTH_LONG).show();
        }
    }

    public HashMap<String, String> getBasicParams() {
        HashMap<String, String> paramMap = new HashMap<String, String>(){{
            put("date", getInputDate());
        }};
        return paramMap;
    }

    public boolean connectionOk() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public HashMap<String, String> getRequestParameters() {
        HashMap<String, String> paramMap = getBasicParams();

        paramMap.put("lat",  "52.629729");
        paramMap.put("lng",  "1.131592");

        return paramMap;
    }

    public ArrayList<String> getCustomCoords() {
        return new ArrayList<String>(){{
            add("52.268,0.543");
            add("52.794,0.238");
            add("52.130,0.478");
        }};
    }

    public HashMap<String, String> getCustomRequestParameters(ArrayList<String> latLngList ) {
        String latLng;
        if( null != latLngList ) {
            latLng = getCrimeCustomLocation(latLngList);
        } else {
            latLng = getCrimeCustomLocation(getCustomCoords());
        }

        HashMap<String, String> paramMap = null;
        if( null != latLng ) {
            paramMap = getBasicParams();
            paramMap.put("poly", latLng);
        }
        return paramMap;
    }

    public String getInputDate() {
        EditText dateText = (EditText) findViewById(R.id.crime_by_date_input);
        String date = dateText.getText().toString();

//        if( null == dateFormatter ) {
//            dateFormatter = new SimpleDateFormat("yyyy-MM", Locale.ENGLISH);
//        }
        return ("" != date) ? date : defaultDate;
    }

    // see https://data.police.uk/docs/method/crime-street/
    public String getCrimeCustomLocation(ArrayList<String> latLngList) {
        StringBuilder latLngBuilder = new StringBuilder();
        String separator = ":";

        if( null != latLngList ) {
            for( String latLngEntry : latLngList ) {
                latLngBuilder.append(latLngEntry);
                latLngBuilder.append(separator);
            }
        } else {
            latLngBuilder = null;
        }

        Log.d("LATITUDE LONGITUDE STRING: ", latLngBuilder.toString().substring(0, latLngBuilder.length()-1));

        // return the full string of coordinates or null
        return (null != latLngBuilder )
            ?   latLngBuilder.toString().substring(0, latLngBuilder.length()-1)
            :   null;
    }

    private void renderCrimesList(String jsonResponse) {
        Log.d("JSON RESPONSE", jsonResponse);

        final ListView crimeRecordsList = (ListView) findViewById(R.id.list_crime_records);

        Gson gson = new Gson();
        try {
            // convert response string to gson for easy handling
            CrimeRecord[] crimeRecords = gson.fromJson(jsonResponse, CrimeRecord[].class);

            crimeRecordsList.setAdapter(new CrimeRecordAdapter(thisActivity, crimeRecords));

        } catch( Exception e ) {
            e.printStackTrace();
            Toast.makeText(this, "Malformed request/response to/from server.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Created by Vanessa_CU on 08/05/2015.
     */
    public class GetRequestTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPostExecute(String jsonResponse) {
            renderCrimesList(jsonResponse);
        }

        @Override
        protected String doInBackground(String... params) {
            // params comes from the execute() call: params[0] is the url.
            try {
                return makeRequest(params[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // Given a URL, establishes an HttpUrlConnection and retrieves
        // the web page content as a InputStream, which it returns as
        // a string.
        private String makeRequest(String fullRequestUrl) throws IOException {
            InputStream inputStream = null;

            try {
                URL url = new URL(fullRequestUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                inputStream = conn.getInputStream();

                // Convert the InputStream into a string
                String contentAsString = readInputStreamContents(inputStream);
                return contentAsString;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }

        // Reads an InputStream and converts it to a String.
        public String readInputStreamContents(InputStream stream)
                throws IOException{
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( stream ));
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();
            while( null != line ) {
                stringBuilder.append(line);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            return stringBuilder.toString();
        }
    }
}

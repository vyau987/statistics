package com.vanessayau.statistics;

import java.util.ArrayList;

/**
 * Class that builds string of lat:lng coords which form a polygon X miles from your current location
 * Created by Vanessa_CU on 12/05/2015.
 */
public class PolygonBuilder {
    private String currentLat, currentLng;
    private int radius;
    private ArrayList<String> latLngList;

    public PolygonBuilder(String latitude, String longitude) {
        this.latLngList = new ArrayList<String>();

        this.currentLat = latitude;
        this.currentLng = longitude;

        this.radius = 20;

        buildPolygon();
    }

    public ArrayList<String> getLatLngList() {
        return latLngList;
    }

    public void buildPolygon() {
        setNorthPoint();
        setEastPoint();
        setSouthPoint();
//        setWestPoint();
    }

    public void setNorthPoint() {
        latLngList.add(getLatitude("n") + "," + currentLng);
        System.out.println("LOCATION: N: " + getLatitude("n") +"," + currentLng);
    }

    public void setSouthPoint() {
        latLngList.add(getLatitude("s") + "," + currentLng);
        System.out.println("LOCATION: S: " + getLatitude("s") +"," + currentLng);
    }

    public void setWestPoint() {
        latLngList.add(currentLat + "," + getLongitude("w"));
        System.out.println("LOCATION: W: " + currentLng + "," + getLatitude("w"));
    }

    public void setEastPoint() {
        latLngList.add(currentLat + "," + getLongitude("e"));
        System.out.println("LOCATION: E: " + currentLng +"," + getLatitude("e"));
    }

    // assume north is negative and south is positive from current lat
    public String getLatitude(String direction) {
        Double newLat;
        Double latDifference = calculateLatDifference();
        if( "n".equalsIgnoreCase(direction) ) {
            newLat = Double.valueOf(currentLat) - latDifference;
        } else {
            newLat = Double.valueOf(currentLat) + latDifference;
        }
        return String.valueOf(newLat);
    }

    // assume west is negative and east is positive from current lng
    public String getLongitude(String direction) {
        Double newLng;
        Double lngDifference = calculateLngDifference();
        if( "w".equalsIgnoreCase(direction) ) {
            newLng = Double.valueOf(currentLat) - lngDifference;
        } else {
            newLng = Double.valueOf(currentLat) + lngDifference;
        }
        return String.valueOf(newLng);
    }

    public double calculateLatDifference() {
        return radius * (1 / 69);
    }

    public double calculateLngDifference() {
        return radius * (1/ 69.172);
    }
}

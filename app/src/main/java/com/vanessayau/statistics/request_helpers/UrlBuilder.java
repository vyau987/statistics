package com.vanessayau.statistics.request_helpers;

import java.util.HashMap;

/**
 * Created by Vanessa_CU on 08/05/2015.
 */
public class UrlBuilder {

    private final static String ROOT_API_URL = "https://data.police.uk/api";
    private String requestUrl;
    private HashMap<String, String> paramMap;

    public UrlBuilder(){};

    public UrlBuilder(String requestUrl, HashMap paramMap) {
        this.requestUrl = requestUrl;
        this.paramMap = paramMap;
    }

    @Override
    public String toString() {
        StringBuilder fullRequestUrlBuilder = new StringBuilder(this.ROOT_API_URL);
        fullRequestUrlBuilder.append("/" + requestUrl);
        if( null != paramMap ) {
            fullRequestUrlBuilder.append("?");
            for( String paramEntry : paramMap.keySet() ) {
                fullRequestUrlBuilder.append(paramEntry);
                fullRequestUrlBuilder.append("=");
                fullRequestUrlBuilder.append(paramMap.get(paramEntry));
                fullRequestUrlBuilder.append("&");
            }
        }
        String fullUrl = fullRequestUrlBuilder.toString();
        return fullUrl.substring(0, fullUrl.length() - 1);
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public void setParamMap(HashMap<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public HashMap<String, String> getParamMap() {
        return paramMap;
    }
}

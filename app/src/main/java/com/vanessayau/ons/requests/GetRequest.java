package com.vanessayau.ons.requests;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;

/**
 * Created by Vanessa_CU on 08/05/2015.
 */
public class GetRequest {
    private String jsonResponse;
    private boolean requestSuccessful;

    public GetRequest(){
        requestSuccessful = true;
    };

    public GetRequest(String url, HashMap<String, String> paramMap) {
        this();

        // generate url
        UrlBuilder urlBuilder = new UrlBuilder(url, paramMap);

        // make get request
        this.handleRequest(urlBuilder.toString());
    }



    // asynchronously issues a network request in a new thread
    public void handleRequest(String url) {
        final HttpGet httpGet = new HttpGet(url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpResponse response = httpClient.execute(httpGet);

                    handleResponse(response);
                } catch( Exception e ) {
                    e.printStackTrace();
                    requestSuccessful = false;
                }
            }
        }).start();
    }

    public void handleResponse(HttpResponse response) {
        StringBuilder responseStringBuilder = null;
        try {
            responseStringBuilder = new StringBuilder(
                    EntityUtils.toString(response.getEntity()) );
        } catch( Exception e ) {
            e.printStackTrace();
            requestSuccessful = false;
        }

        if (null != responseStringBuilder) {
            setJsonResponse( responseStringBuilder.toString() );
        }
    }

    public String getJsonResponse() {
        return jsonResponse;
    }

    public void setJsonResponse(String jsonResponse) {
        this.jsonResponse = jsonResponse;
    }

    public void setRequestSuccessful(boolean requestSuccessful) {
        this.requestSuccessful = requestSuccessful;
    }

    public boolean getRequestSuccessful() {
        return this.requestSuccessful;
    }
}

package com.vanessayau.ons.activities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.vanessayau.ons.R;
import com.vanessayau.ons.messages.ResponseMessage;
import com.vanessayau.ons.requests.UrlBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends Activity {
    private ArrayList<ResponseMessage.CrimeRecord> crimeRecords;
    public ArrayAdapter<ResponseMessage.CrimeRecord> crimeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button makeRequestButton = (Button) findViewById(R.id.button_make_request);
        makeRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeRequest();
            }
        });
    }

    public void makeRequest() {
        if( connectionOk() ) {
            String url = "crimes-street/all-crime";
            String fullUrl = (new UrlBuilder(url, getRequestParameters())).toString();
            new GetRequestTask().execute(fullUrl);
        } else {
            Toast.makeText(this, "No network connection available.", Toast.LENGTH_LONG).show();
        }
    }

    private void renderCrimesList(String jsonResponse) {
        ResponseMessage jsonMessage = new ResponseMessage(jsonResponse);

        for( ResponseMessage.CrimeRecord crimeRecord : jsonMessage.getCrimeRecords() ) {
            //TODO generate listview items containing information
            System.out.println("CATEGORY: " + crimeRecord.getCategory());
            System.out.println("LOCATION: " + crimeRecord.getLocation());
        }
    }

    public boolean connectionOk() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public HashMap<String, String> getRequestParameters() {
        HashMap<String, String> paramMap = new HashMap<String, String>(){{
            put("date", "2013-01");
            put("lat",  "52.629729");
            put("lng",  "1.131592");
        }};
        return paramMap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Created by Vanessa_CU on 08/05/2015.
     */
    public class GetRequestTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPostExecute(String jsonResponse) {
            renderCrimesList(jsonResponse);
            Log.d("JSON RESPONSE AS STRING",jsonResponse);
        }

        @Override
        protected String doInBackground(String... params) {
            // params comes from the execute() call: params[0] is the url.
            try {
                return makeRequest(params[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // Given a URL, establishes an HttpUrlConnection and retrieves
        // the web page content as a InputStream, which it returns as
        // a string.
        private String makeRequest(String fullRequestUrl) throws IOException {
            InputStream inputStream = null;

            try {
                URL url = new URL(fullRequestUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                inputStream = conn.getInputStream();

                // Convert the InputStream into a string
                String contentAsString = readInputStreamContents(inputStream);
                return contentAsString;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }

        // Reads an InputStream and converts it to a String.
        public String readInputStreamContents(InputStream stream)
                throws IOException{
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( stream ));
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();
            while( null != line ) {
                stringBuilder.append(line);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            return stringBuilder.toString();
        }
    }
}

package com.vanessayau.ons.messages;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Vanessa_CU on 08/05/2015.
 */
public class ResponseMessage {
    private ArrayList<CrimeRecord> crimeRecords;

    public ResponseMessage(String jsonResponse) {
        ArrayList<CrimeRecord> crimeRecords = new ArrayList<CrimeRecord>();

        try {
            JSONArray jsonArray = new JSONArray(jsonResponse);
            for( int i=0 ; i<jsonArray.length() ; i++ ) {
                crimeRecords.add(getCrimeRecord(jsonArray.getJSONObject(i)));
            }
        } catch( Exception e ) {
            e.printStackTrace();
        }
        this.crimeRecords = crimeRecords;
    }

    public ArrayList<CrimeRecord> getCrimeRecords() {
        return crimeRecords;
    }

    private CrimeRecord getCrimeRecord(JSONObject crimeInfo) {
        CrimeRecord crimeRecord;
        try{
            String category = crimeInfo.getString("category");
            String location = crimeInfo
                    .getJSONObject("location")
                    .getJSONObject("street")
                    .getString("name");
            crimeRecord = new CrimeRecord(category, location);
        } catch( Exception e ) {
            crimeRecord = null;
            Log.d("ERROR", "method getCrimeRecord");
            e.printStackTrace();
        }

        return crimeRecord;
    }

    public class CrimeRecord {
        private String location;
        private String category;

        public CrimeRecord() {}

        public CrimeRecord(String category, String location) {
            this.category = category;
            this.location = location;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }
    }
}
